/* 1. Create a view called "sales_revenue_by_category_qtr" that shows the film category and total sales revenue for the current quarter. 
 * The view should only display categories with at least one sale in the current quarter. 
 * The current quarter should be determined dynamically. 
 */

CREATE VIEW sales_revenue_by_category_qtr AS
SELECT c."name"  AS category, SUM(p.amount) AS total_sales_revenue
FROM category c
JOIN film_category fc ON c.category_id = fc.category_id
JOIN film f ON fc.film_id = f.film_id
JOIN inventory i ON f.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
JOIN payment p ON r.rental_id = p.rental_id
WHERE EXTRACT(QUARTER FROM p.payment_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
GROUP BY c."name" 
HAVING SUM(p.amount) > 0;

/* 2. Create a query language function called "get_sales_revenue_by_category_qtr" that accepts one parameter representing 
 * the current quarter and returns the same result as the "sales_revenue_by_category_qtr" view.
 */

CREATE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INTEGER)
RETURNS TABLE(category_name TEXT, revenue NUMERIC)
AS $$
BEGIN
    RETURN QUERY SELECT category.name, SUM(payment.amount) AS revenue
    FROM payment
    JOIN rental ON payment.rental_id = rental.rental_id
    JOIN inventory ON rental.inventory_id = inventory.inventory_id
    JOIN film_category ON inventory.film_id = film_category.film_id
    JOIN category ON film_category.category_id = category.category_id
    WHERE EXTRACT(QUARTER FROM rental.rental_date) = current_quarter
    GROUP BY category.name;
END;
$$ LANGUAGE plpgsql;

/* 3. Create a procedure language function called "new_movie" that takes a movie title as a parameter and inserts a new movie with the given title in the film table. 
 * The function should generate a new unique film ID, set the rental rate to 4.99, the rental duration to three days, 
 * the replacement cost to 19.99, the release year to the current year, and "language" as Klingon. 
 * The function should also verify that the language exists in the "language" table. 
 * Then, ensure that no such function has been created before; if so, replace it.
 */

CREATE OR REPLACE FUNCTION new_movie(p_title text)
RETURNS VOID AS $$
DECLARE
    v_language_id INT;
    v_film_id INT;
BEGIN
    SELECT MAX(film_id) + 1 INTO v_film_id FROM film;

    v_language_id := (SELECT language_id FROM "language" WHERE name = 'Klingon');

    IF v_language_id IS NULL THEN
        RAISE EXCEPTION 'Language does not exist.';
    END IF;
    
    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (v_film_id, p_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), v_language_id);
    
END;
$$ LANGUAGE plpgsql;

INSERT INTO "language" ("name") VALUES ('Klingon');

SELECT * FROM new_movie('Any Movie Title');

SELECT * FROM film; 
 